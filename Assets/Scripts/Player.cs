using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : BaseCharacter
{
    Camera mainCamera;

    public Transform firePoint;
    Vector3 moveInput;
    Vector3 moveVelocity;

    [SerializeField] float moveSpeed;

    bool isFiring;

    protected override void Start()
    {
        base.Start();

        mainCamera = FindObjectOfType<Camera>();
    }

    void Update()
    {
        Movement();
        PointingGun();

        nextFire -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            isFiring = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            isFiring = false;
        }

        if (isFiring)
        {
            if (nextFire <= 0)
            {
                nextFire = fireRate;
                FiringMove();
            }
        }
    }

    void FixedUpdate()
    {
        rb.velocity = moveVelocity;
    }

    protected override void Movement()
    {
        base.Movement();

        moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput * moveSpeed;
    }

    void PointingGun()
    {
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane ground = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if (ground.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength);

            transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
        }
    }

    protected override void FiringMove()
    {
        base.FiringMove();

        bulletPooling.Request(firePoint);
        bulletPooling.tag = "Player";
    }
}
