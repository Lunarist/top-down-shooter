using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Character character;
    PlayerController player;
    protected BulletPooling bulletPooling;

    Rigidbody rb;

    float moveSpeed;
    float fireRate;
    float nextFire;

    public Transform firePoint;
    [HideInInspector]  public int damage;
    int currentHealth;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = FindObjectOfType<PlayerController>();
        bulletPooling = FindObjectOfType<BulletPooling>();

        currentHealth =  character.healthPoint;
        damage = character.damage;
        fireRate = character.fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform.position);

        if (currentHealth <= 0)
        {
            Death();
        }

        nextFire -= Time.deltaTime;
        if (nextFire <= 0) 
        {
            nextFire = fireRate;
            FiringMove();
        }
        
    }

    protected virtual void FiringMove()
    {
        Debug.Log("Is Firing");
    }

    public void GotHurt(int dmg)
    {
        currentHealth -= dmg;
    }

    void Death()
    {
        //SpawnEnemy.enemyPool.Release(this);
        Destroy(gameObject);
    }
}
