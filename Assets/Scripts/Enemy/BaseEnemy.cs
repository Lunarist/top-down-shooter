using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : BaseCharacter
{
    public Transform firePoint;

    PlayerController player;

    public Character character;

    SpawnEnemy spawnEnemy;

    // Start is called before the first frame update
    protected override void Start()
    {
        player = FindObjectOfType<PlayerController>();
        maxHP = character.healthPoint;
        fireRate = character.fireRate;
        nextFire = fireRate;
        spawnEnemy = FindObjectOfType<SpawnEnemy>();

        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform.position);

        nextFire -= Time.deltaTime;
        if (nextFire <= 0) 
        {
            nextFire = fireRate;
            // FiringMove();
        }

        if (currHP <= 0)
        {
            Death();
        }
    }

    protected override void Movement()
    {
        base.Movement();
    }

    protected override void FiringMove()
    {
        base.FiringMove();
    
        bulletPooling.Request(firePoint);
        bulletPooling.tag = "Enemy";
    }

    protected override void Death()
    {
        base.Death();
        spawnEnemy.Remove(this); 
        currHP = maxHP;
    }
}
