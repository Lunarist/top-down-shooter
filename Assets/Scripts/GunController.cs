using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public PlayerManager player;
    public BulletPooling bulletPooling;
    public Transform firePoint;

    public bool isFiring;
    
    float fireRate;
    float nextFire;

    // Start is called before the first frame update
    void Start()
    {
        fireRate = player.character.fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        nextFire -= Time.deltaTime;

        if (isFiring)
        {
            if (nextFire <= 0)
            {
                nextFire = fireRate;
                // Bullet bullet = bulletPooling.Request(firePoint.transform.position);

                // bulletPooling.SetTransform(firePoint);
                // BulletPooling.bulletPool.Get();
            }
        }
    }
}
