using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class SpawnEnemy : MonoBehaviour
{
    ObjectPool<BaseEnemy> enemyPool;

    public BaseEnemy enemy;

    public Transform parent;

    float nextSpawn;
    float spawnTime = 1.0f;

    public BoxCollider spawnArea;
    Bounds bounds;
     
    // Start is called before the first frame update
    void Start()
    {
        enemyPool = new ObjectPool<BaseEnemy>
        (
            CreatePooledItem,
            OnTakeFromPool,
            OnReturnedToPool,
            OnDestroyPoolObject,
            false,
            3,
            5
        );

        bounds = GetComponent<BoxCollider>().bounds;
        nextSpawn = spawnTime;
    }

    private void OnDestroyPoolObject(BaseEnemy obj)
    {
        Destroy(obj.gameObject);
    }

    private void OnReturnedToPool(BaseEnemy obj)
    {
        obj.gameObject.SetActive(false);
    }

    private void OnTakeFromPool(BaseEnemy obj)
    {
        obj.gameObject.SetActive(true);
    }

    private BaseEnemy CreatePooledItem()
    {
        BaseEnemy newEnemy = Instantiate(enemy);
        newEnemy.transform.SetParent(parent);
        return newEnemy;
    }

    // Update is called once per frame
    void Update()
    {
        nextSpawn -= Time.deltaTime;
        if (nextSpawn <= 0)
        {
            nextSpawn = spawnTime;
            Request(SetSpawnPos());
        }
    }

    BaseEnemy Request(Vector3 spawnPos)
    {
        BaseEnemy newEnemy = enemyPool.Get();
        newEnemy.transform.position = spawnPos;
        return newEnemy;
    }

    public void Remove(BaseEnemy instance)
    {
        enemyPool.Release(instance);
    }

    Vector3 SetSpawnPos()
    {
        Vector3 spawnPos = new Vector3(UnityEngine.Random.Range(-bounds.extents.x, bounds.extents.x), 
                                        1, 
                                        UnityEngine.Random.Range(-bounds.extents.z, bounds.extents.z));

        return spawnPos;
    }
}
