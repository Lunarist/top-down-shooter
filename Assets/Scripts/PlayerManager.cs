using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public Character character;

    int damage;
    int currHealth;

    // Start is called before the first frame update
    void Start()
    {
        currHealth = character.healthPoint;
        damage = character.damage;
    }

    // Update is called once per frame
    void Update()
    {
        if (currHealth <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    public void GotHurt(int dmg)
    {
        dmg = damage;
        currHealth -= dmg;
    }
}
