using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    [SerializeField] protected float lifeTime;
    protected float currTime;

    protected BulletPooling bulletPooling;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        currTime = lifeTime;
        bulletPooling = FindObjectOfType<BulletPooling>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        BulletMovement();
    }

    //protected virtual void BulletMovement()
    //{
    //    // Set Speed for every type of bullet
    //}

    void BulletMovement()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
}
