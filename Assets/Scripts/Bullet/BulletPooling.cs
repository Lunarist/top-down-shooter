using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class BulletPooling : MonoBehaviour
{
    ObjectPool<Bullet> bulletPool;

    public Bullet bullet;
    public Transform parent;

    // Start is called before the first frame update
    void Start()
    {
        bulletPool = new ObjectPool<Bullet>
        (
            CreatePooledItem, 
            OnTakeFromPool, 
            OnReturnedToPool, 
            OnDestroyPoolObject, 
            false,
            10,
            20
        );
    }

    Bullet CreatePooledItem()
    {
        Bullet instance = Instantiate(bullet, parent);
        return instance;
    }

    void OnTakeFromPool(Bullet instance)
    {
        instance.gameObject.SetActive(true);
    }

    void OnReturnedToPool(Bullet instance)
    {
        instance.gameObject.SetActive(false);
    }

    void OnDestroyPoolObject(Bullet instance)
    {
        Destroy(instance.gameObject);
    }

    public Bullet Request(Transform firePoint)
    {
        Bullet instance = bulletPool.Get();
        instance.transform.position = firePoint.position;
        instance.transform.rotation = firePoint.rotation;
        return instance;
    } 

    public void Remove(Bullet instance)
    {
        bulletPool.Release(instance);
    }
}
