using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : Bullet
{
    protected override void Start()
    {
        base.Start();
    }
    
    protected override void Update()
    {
        //BulletMovement();
        base.Update();

        currTime -= Time.deltaTime;
        if (currTime <= 0)
        {
            currTime = lifeTime;
            bulletPooling.Remove(this);
        }
    }

    //protected override void BulletMovement()
    //{
    //    transform.Translate(Vector3.forward * speed * Time.deltaTime);
    //}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Enemy" && bulletPooling.tag == "Player")
        {
            bulletPooling.Remove(this);
            other.gameObject.GetComponent<BaseEnemy>().GotHurt(1);
            Debug.Log("Shooting Enemy");
        }

        if (other.gameObject.tag == "Player" && bulletPooling.tag == "Enemy")
        {
            bulletPooling.Remove(this);
            other.gameObject.GetComponent<Player>().GotHurt(1);
            Debug.Log("Shooting Player");
        }
    }
}
