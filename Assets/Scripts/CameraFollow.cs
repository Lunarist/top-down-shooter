using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;

    float speed = 0.1f;
    Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.position;
    }

    void FixedUpdate()
    {
        Vector3 finalPosition = player.position + offset;
        Vector3 lerpPosition = Vector3.Lerp (transform.position, finalPosition, speed);
        transform.position = lerpPosition;
    }
}
